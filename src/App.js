import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Navbar from "./layouts/Navbar";
import Footer from "./layouts/Footer";
import List from "./components/List";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <div className="container mt-4 mb-4">
          <Route exact path="/" component={List} />        
          {/* <Search /> */}
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
