import React, { Component } from "react";
import axios from "axios";
import _ from "lodash";

class List extends Component {
  state = {
    search_list: [],
    searchVal:""
  }

  componentDidMount() {
    axios.get(`https://itunes.apple.com/in/rss/topalbums/limit=100/json`)
      .then(({ data }) => {              
        this.setState({
          search_list: data.feed.entry
        })
      })      
      .catch(function (error) {
        console.log(error);
      });
  }
  
  searchAlbum(e) {
    this.setState({ searchVal: e.target.value })
  }

  render() {  
    
    var search = this.state.searchVal;
    var results = _.filter(this.state.search_list, function(item) {
      return _.toLower(item.title.label).indexOf(_.toLower(search)) !== -1;
    });

    const top_list_show = results && results.map((item, key) => {
        return <div className="col-4 mb-5">
            <div className="d-flex justify-content-center">
              <img src={item["im:image"][2]["label"]} alt={item.id.label} className="img-fluid" />
            </div>
            <p className="text-center pt-2">
              <small>{item["title"]["label"]}</small>
            </p>
          </div>;
      });
    
    return <div className="row">
        <div className="col-12 mb-5">
          <input type="text" onChange={e => this.searchAlbum(e)} placeholder="Search" className="form-control" />
        </div>
        {results.length===0 ? <p className="text-center">No results found.</p> : top_list_show}
      </div>;
  }
}

export default List;
